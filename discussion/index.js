// why leaf is the sign of mongo - natural and simple

//Node.js -> will provide us with a runtime environment (RTE) that will allow us to execute our program.

//RTE (Runtime Environment) -> is the environment/system in which a program can be executed.

//TASK: Lets create a standard server setup using NodeJS

//Identify and prepare the components/ ingredients needed in order to execute the task.
	// https (hypertext transfer protocol)
	// http -> is a built in module of NodeJS. that will allow us to establish a connection and allow us 
	// to transfer data over the HTTP.

	//use a function called require() in order to acquire the desired module.
	// Repackage the module and store it inside a new variable.

	let http = require('http');

		//http will provide us with all the components needed to establish a server.

		//createServer() -> will allow us to create an HTTP server.

		// provide/ instruct the server what to do.
		// instruct to print a simple message.

		// Identify and describe a location where the connection will happen, in order to provide 
		// a proper medium for both parties. and bind the connection to the desired port number.
		//- declare a variable that describes an address
	let port = 3000;

		//listen() -> bind and listen to a specific port whenever its being accessed by your computer.

		//identify a point where we want to terminate the transmission using the end() function

	http.createServer(function(request, response){
		response.write('Hello Batch 156, Welcome to Backend!');
		response.end();

		 //terminate the transmission of sending the message without
		 // completely shutting down the entire network
	}).listen(port);

		// create a response in the terminal to confirm if a connection has been successfully established.
		// 1. we can give a confirmation to the client that a connection is working.
		// 2. we can specify what address/location the connection was established.
		// we will use template literals in order to utilize placeholders.

console.log(`Server is running with nodemon on port: ${port}`);

	// What can we do for us to be able to automatically hotfix the system without restarting the server

	//1. initialize a npm intoyour local project.
		//syntax: npm init
		//shortcut syntax: npm init -y (yes)

		// package.json -> "the Heart of any Node Projects". it records all the important metadata about
		// the project, (libraries, packages, functions) that make up system.

	//2. install a dependency 'nodemon' into our project
		//PRACTICE SKILLS:
			// install:
				//npm install <dependency>
				// npm i <dependency>
			// uninstall:
				//npm uninstall <dependency>
				//npm un <dependency>
			//installing multiple dependencies:
				//npm i <list of dependencies>
					//express
	//3. run your app using the nodemon utility engine.
		//note: make sure that nodemon is recognized by your file system structure
		// npm install -g nodemon (global)
			//-> the library is being installed "globally" into your 
			// machine's file system so that the program will be recognizable accross your
			// file system.
			// (*YOU ONLY HAVE TO EXECUTE THIS COMMAND ONCE.*)
		//nodemon app.js

		//nodemon utility -> is a CLI utility tool, its task is to wrap your 
		// node js app and watch for any changes in the file system and automatically
		// restarts/hotfix the process.

		//create your personalized script to execute the app using the nodemon utility.
			//register a new command to start the app.
			//start -> common execute script for node apps.
			//others -> npm run + command

			//if nodemon is pushed, with or more dependencies = bumibigat if will be pushed


	//[SEGWAY] Register sublime Text 3 as part of your environment variables.

		// 1. Locate in your PC where Sublime Text 3 is installed. 
		// 2. Copy the path where sublime text 3 is installed
		// C:\Program Files\Sublime Text
		// 3. Locate the env variables in your system
		// 4. Insert the path of sublime 3